#! /usr/bin/env bash

# Define the options and their corresponding numbers
options=("Standart" "Mathe" "Deutsch" "Englisch" "Berufswahl" "Mathe Erweiterug" "Deutsch Festigung" "Verschiedenes")
numbers=("0" "1" "2" "3" "4" "5" "6" "7")

# Display options in dmenu and get the selection
selected=$(printf "%s\n" "${options[@]}" | /usr/local/bin/dmenu -i -c -l 30)

# Find the index of the selected option and get the corresponding number
for i in "${!options[@]}"; do
    if [[ "${options[$i]}" == "$selected" ]]; then
        layer_full_info="${numbers[$i]}"
        break
    fi
done

# Test the result (optional)
#echo "Selected option: $selected"
#echo "Corresponding number: $layer_full_info"

notify-send "Layer $layer_full_info" "setting new layer to $layer_full_info" &
herbstclient setenv HLWM_CURRENT_LAYER $layer_full_info

. /home/csabi/.config/herbstluftwm/csripts/switch_layer_without_interaction.sh
