#! /usr/bin/env bash

hc() {
		herbstclient "$@"
}

############################
# this is the layer switcher
############################

Mod=Mod4

current_layer=`hc getenv HLWM_CURRENT_LAYER`

############################
# change the color of
# windows (as an indicator
# for the layer being used)
############################
case $current_layer in

	0)
		# default layer
		hc attr theme.border_width 5
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s Default  || alacritty -e tmux new -A -s Default  || urxvt -e tmux new -A -s Default
		hc attr theme.active.color '#ffff00'
		;;

	1)
		# Mathe
		hc attr theme.border_width 5
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s Mathe  || alacritty -e tmux new -A -s Mathe  || urxvt -e tmux new -A -s Mathe
		hc attr theme.active.color '#008800'
		;;

	2)
		# Deutsch
		hc attr theme.border_width 5
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s Deutsch  || alacritty -e tmux new -A -s Deutsch  || urxvt -e tmux new -A -s Deutsch
		hc attr theme.active.color '#ff8800'
		;;


	3)
		# Englisch
		hc attr theme.border_width 5
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s Englisch  || alacritty -e tmux new -A -s Englisch  || urxvt -e tmux new -A -s Englisch
		hc attr theme.active.color '#0088ff'
		;;

	4)
		# Berufliche Orientierung
		hc attr theme.border_width 5
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s BeruflicheOrientierung  || alacritty -e tmux new -A -s BeruflicheOrientierung  || urxvt -e tmux new -A -s BeruflicheOrientierung
		hc attr theme.active.color '#0088ff'
		;;


	5)
		# Mathe Erweiterung
		hc attr theme.border_width 5
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s MatheErweiterung  || alacritty -e tmux new -A -s MatheErweiterung  || urxvt -e tmux new -A -s MatheErweiterung
		hc attr theme.active.color '#ffffff'
		;;


	6)
		# Deutsch Festigung
		hc attr theme.border_width 5
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s DeutschFestigung  || alacritty -e tmux new -A -s DeutschFestigung  || urxvt -e tmux new -A -s DeutschFestigung
		hc attr theme.active.color '#ffffff'
		;;

	7)
		# Verschiedenes
		hc attr theme.border_width 5
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s Verschiedenes  || alacritty -e tmux new -A -s Verschiedenes  || urxvt -e tmux new -A -s Verschiedenes
		hc attr theme.active.color '#ffffff'
		;;

	*)
		# Sollte nie passieren, aber falls ja:
		hc attr theme.border_width 50
		hc keybind $Mod-Return spawn kitty -e tmux new -A -s KOMISCH  || alacritty -e tmux new -A -s KOMISCH  || urxvt -e tmux new -A -s KOMISCH
		hc attr theme.active.color '#880000'
		;;
esac


tag_names=( {0..4} )
tag_keys=(  d h t n s  )

for i in ${!tag_names[@]} ; do
	key="${tag_keys[$i]}"
	if ! [ -z "$key" ] ; then
		hc keyunbind "$Mod-$key"
		hc keybind "$Mod-$key" use_index "$current_layer$i" # ... but only map keys for navigating the first layer
		hc keyunbind "$Mod-Shift-$key"
		hc keybind "$Mod-Shift-$key" move_index "$current_layer$i"
	fi
done

fifth=5
hc keyunbind $Mod-minus
hc keybind $Mod-minus use_index "$current_layer$fifth"
hc keyunbind $Mod-Shift-minus
hc keybind $Mod-Shift-minus move_index "$current_layer$fifth"

############################
zero=0
one=1
two=2
three=3
four=4
five=5

# notify-send "printing current tag names"
# notify-send $current_layer$zero
# notify-send $current_layer$one
# notify-send $current_layer$two
# notify-send $current_layer$three
# notify-send $current_layer$four
# notify-send $current_layer$five

# place windows on tags
hc rule focus=on # normally focus new clients
hc rule class="Firefox" tag=$current_layer$one
hc rule class="Chromium" tag=$current_layer$one
hc rule class="Brave" tag=$current_layer$one
hc rule class="Brave-browser" tag=$current_layer$one
hc rule class="brave-browser" tag=$current_layer$one
hc rule class="firefox" tag=$current_layer$one
hc rule class="Abrowser" tag=$current_layer$one
hc rule windowrole="GtkFileChooserDialog" fullscreen="on"
#hc rule instance="Save File" tag=$current_layer$two
#hc rule class="qutebrowser" tag=$current_layer$one
hc rule class="URxvt" tag=$current_layer$two
hc rule class="kitty" tag=$current_layer$two
hc rule class="Gvim" tag=$current_layer$three
hc rule class="Emacs" tag=$current_layer$three
#hc rule window_role="conversation" tag=floating enable, move container to workspace 05
hc rule window_role="conversation" tag=$current_layer$five
hc rule class="St" tag=$current_layer$two
#hc rule class="Evince" tag=$current_layer$four
hc rule class="Tor Browser" tag=$current_layer$zero
hc rule class="libreoffice" tag=$current_layer$zero
hc rule class="MPlayer" tag=$current_layer$four

############################
