pactl set-sink-volume alsa_output.pci-0000_00_09.2.analog-stereo 200%
pactl set-sink-volume alsa_output.pci-0000_00_1f.3.analog-stereo 200%
notify-send `amixer sget Master|grep %|grep Left | awk -F'[' '{print $2}' | awk -F']' '{print $1}'`
