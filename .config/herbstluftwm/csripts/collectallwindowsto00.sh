#!/bin/bash

# Iterate over tags from 00 to 69
for tag in $(seq -w 0 69); do
    # Get the clients for the current tag
    tag_clients=$(herbstclient list_clients --tag="$tag" 2>/dev/null)
    
    # Bring each client to the current tag (now 00)
    while read -r client_id; do
        if [ -n "$client_id" ]; then
            herbstclient bring "$client_id"
        fi
    done <<< "$tag_clients"
done
