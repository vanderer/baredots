amixer -q set Master toggle
notify-send `amixer sget Master|grep %|grep Left | awk -F'[' '{print $3}' | awk -F']' '{print $1}'`
