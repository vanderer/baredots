#!/usr/bin/env bash

# Initialize an empty list for clients
clients=""

# Iterate over tags from 00 to 69
for tag in $(seq -w 0 69); do
    # Get the clients for the current tag
    tag_clients=$(herbstclient list_clients --tag="$tag" 2>/dev/null)
    
    # Iterate through each client to get its window name
    while read -r client_id; do
        # Get the window title of the client
        window_name=$(herbstclient get_attr clients."$client_id".title 2>/dev/null)
        
        # Append to the clients list in the format "window_name (tag_id):client_id"
        if [ -n "$window_name" ]; then
            clients+="$window_name (tag $tag):$client_id:$tag"$'\n'
        fi
    done <<< "$tag_clients"
done

# Check if any clients were found
if [ -z "$clients" ]; then
    echo "No clients found!" >&2
    exit 1
fi

# Show the list in dmenu and get the user's choice
selected=$(echo "$clients" | dmenu -i -l 10 -p "Select a window:")

# If no selection, exit
[ -z "$selected" ] && exit

# Extract the client ID and tag from the selected option
client_id=$(echo "$selected" | awk -F':' '{print $2}')
tag=$(echo "$selected" | awk -F':' '{print $3}')

# Switch to the tag of the selected client
herbstclient use "$tag"

# Focus the selected client
herbstclient jumpto "$client_id"

