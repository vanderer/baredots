#! /usr/bin/env zsh

herbstclient use_index $( print $(herbstclient tag_status) | sed 's/ /\n/g' | grep [#:\!] | /usr/local/bin/dmenu -l 5 | sed 's/^.//')
