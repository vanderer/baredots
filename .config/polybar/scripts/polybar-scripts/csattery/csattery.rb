#!/usr/bin/env ruby
time=0.0

["BAT0"].each do |battery|
  amount=`cat /sys/class/power_supply/#{battery}/energy_now`.to_f
  voltage=`cat /sys/class/power_supply/#{battery}/voltage_now`.to_f
  time+=amount/voltage
end

puts " | #{time.to_i}:#{((time-time.to_i)*60).to_i.to_s.rjust(2, "0")} ]"
