require 'cairo'

function conky_test()
    return "Hello, Conky!"
end

function conky_draw_text_with_background(text, x, y)
    if conky_window == nil then return end

    -- Create a drawing surface and context
    local cs = cairo_xlib_surface_create(conky_window.display, conky_window.drawable, conky_window.visual, conky_window.width, conky_window.height)
    local cr = cairo_create(cs)

    -- Ensure proper font setup
    cairo_select_font_face(cr, "DejaVu Sans Mono", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL)
    cairo_set_font_size(cr, 12)

    -- Set up the background color (red with opacity)
    local padding = 5  -- Add padding around the text
    local extents = cairo_text_extents_t:create()
    cairo_text_extents(cr, text, extents)

    -- Draw the rectangle behind the text
    cairo_set_source_rgba(cr, 1, 0, 0, 0.8)  -- Red background with 80% opacity
    cairo_rectangle(cr, x - padding, y - extents.height - padding, extents.width + 2 * padding, extents.height + 2 * padding)
    cairo_fill(cr)

    -- Set up the text color (white)
    cairo_set_source_rgba(cr, 1, 1, 1, 1)  -- White text
    cairo_move_to(cr, x, y)
    cairo_show_text(cr, text)
    cairo_stroke(cr)

    -- Clean up
    cairo_destroy(cr)
    cairo_surface_destroy(cs)
end
