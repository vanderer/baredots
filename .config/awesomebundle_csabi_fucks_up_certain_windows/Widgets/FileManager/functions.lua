--THIS JUST WORKS !! DEAL WITH IT
function getDirectoryEntries(path) -- returs table of absolute paths
  -- path must be path to a directory and ending with a slash!!
  local res = {}

  local str = awful.util.pread("ls -1 --file-type '" .. path .. "'")

  for line in string.gmatch(str, "[^\n]+") do
    table.insert(res, path .. line)
  end

  return res
end

function makeNiceName(name)
  local local_name = ""

  if isDir(name) then
    local_name = string.match(name, "/[^/]*/[^/]*$") --/the/whole/name/ --> /name/
    local_name = string.sub(local_name, 2, string.len(local_name)-1) -- /name/ --> name
  else
    local_name = string.match(name, "/[^/]*$") --/the/whole/name --> /name
    local_name = string.sub(local_name, 2, string.len(local_name)) -- /name --> name
  end

  return local_name
end

function splitPath(currentFileManagerPath)
  pathStack = {}
  for partOfPath in string.gmatch(currentFileManagerPath, "[^/]+") do
    if line ~= "lo" then
      table.insert(pathStack, partOfPath)
    end
  end
  return pathStack
end

function stackPop()
  table.remove(stack)
end

function stackPush(element)
  table.insert(fileManagerPathStack, element)
end

function shiftLeft()
  stackPush(element)
  --fileManager.
end

function shiftRight(element)
  stackPop()
  hiddenPreviousPath=
  for i= 1,numberOfEntries do
    fileManager.textbox.directory[i]:set_markup(fileManager.content.current[i])
    fileManager.textbox.current[i]:set_markup(fileManager.content.previous[i])
    fileManager.textbox.previous[i]:set_markup(makeNiceName(getDirectoryEntries(hiddenPreviousPath)[i]))
  end
end

function navigate(current_entry)
 local grabber = awful.keygrabber.run(
 function(mod, key, event)
   if event == "release" then return end

   if     key == 'Up'   then selectPreviousEntry() or scrollUp() or ignoreKey()
   elseif key == 'Down' then selectNextEntry() or scrollDown() or ignoreKey()
   elseif key == 'Right' then shiftLeft()
   elseif key == 'Left'  then shiftRight()
   else   awful.keygrabber.stop(grabber)
   end

 end)
end
