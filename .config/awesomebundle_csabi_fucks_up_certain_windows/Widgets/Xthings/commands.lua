awful.util.spawn("setxkbmap -option -layout " .. myKeyboardLayout .. " -variant " .. myKeyboardLayoutVariant .. " -option lv3:ralt_switch -option shift:both_capslock")
awful.util.spawn("feh --bg-scale " .. awesome_root .. "/Themes/" .. chosenTheme .. "/background.png")
awful.util.spawn("feh --bg-scale " .. awesome_root .. "/Themes/" .. chosenTheme .. "/background.jpg")
awful.util.spawn("xset -b")

-- Various autostarts
startProg("setxkbmap us -variant dvp -option lv3:ralt_switch -option caps:escape -option shift:both_capslock", "")
startProg("/home/csabi/.screenlayout/OneBigTV.sh", "")
startProg("xset -b", "")
startProg("xset r rate 175 200", "")
startProg("xset -dpms", "")
startProg("xset s off", "")
startProg("xinput set-prop \"TPPS/2 IBM TrackPoint\" \"Evdev Wheel Emulation\" 1", "")
startProg("xinput set-prop \"TPPS/2 IBM TrackPoint\" \"Evdev Wheel Emulation Button\" 2", "")
startProg("xinput set-prop \"TPPS/2 IBM TrackPoint\" \"Evdev Wheel Emulation Timeout\" 200", "")
startProg("xinput set-prop \"TPPS/2 IBM TrackPoint\" \"Evdev Wheel Emulation Axes\" 6 7 4 5", "")

startProg("mpd", "")
startProg("megasync", "")
startProg("/home/csabi/.csonfig/Scripts/wallpaper.sh", "")
startProg("syncthing-gtk", " -m")
startProg("compton", " -f")

