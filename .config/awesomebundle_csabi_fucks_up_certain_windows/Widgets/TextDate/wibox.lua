myDateWibox = {}

for s=1, screen.count() do
  myDateWibox[s]         = wibox({})
  myDateWibox[s].width   = 60
  myDateWibox[s].height  = 60
  myDateWibox[s].visible = true
  myDateWibox[s].ontop   = false

  wiboxPositioning(myDateWibox[s], s, 0, 80, "left", "top")

  local marginLayout = wibox.layout.margin()
  marginLayout:set_margins(8)
  marginLayout:set_widget(clockwidget)
  myDateWibox[s]:set_widget(marginLayout)
end
