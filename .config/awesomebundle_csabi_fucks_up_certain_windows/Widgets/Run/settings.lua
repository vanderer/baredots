runBorderWidth  = 1
runPrompt       = " Run: "

runColor        = {}
runColor.fg     = easyTheme.lightColor
runColor.bg     = easyTheme.darkColor
runColor.border = easyTheme.lightColor
runColor.cursor = easyTheme.lightColor
