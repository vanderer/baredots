
trayWibox = wibox({})
trayWibox.height = 25
trayWibox.width = easyTheme.barWidth
trayWibox.visible = true
--trayWibox.ontop = true
trayWibox.bg = "#ff0000"

--wiboxPositioning(trayWibox, 1, 0, 0, "left", "top", "w")
trayWibox.x = 0
trayWibox.y = 240

local layout = wibox.layout.margin()
layout:set_margins(6)
layout:set_widget(tray)

trayWibox:set_widget(layout)

