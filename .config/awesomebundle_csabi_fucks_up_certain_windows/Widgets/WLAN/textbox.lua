wlanTextbox = wibox.widget.textbox()
wlanTextbox:set_text("Setting up ...")
wlanTextbox:set_font("Bitstream Vera Sans Mono, 8")
wlanTextbox:set_valign("top")

vicious.register(wlanTextbox, vicious.widgets.wifi, wlanFunction, 3, wirelessInterface)

wlanWidget = wibox.widget.background()
wlanWidget:set_widget(wlanTextbox)
