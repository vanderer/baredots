myClockWibox = {}

for s=1, screen.count() do
  myClockWibox[s]         = wibox({})
  myClockWibox[s].width   = 60
  myClockWibox[s].height  = 60
  myClockWibox[s].visible = true
  myClockWibox[s].ontop   = false

  wiboxPositioning(myClockWibox[s], s, 0, 90, "left", "top")

  local marginLayout = wibox.layout.margin()
  marginLayout:set_margins(8)
  marginLayout:set_widget(clockwidget)
  myClockWibox[s]:set_widget(marginLayout)
end
