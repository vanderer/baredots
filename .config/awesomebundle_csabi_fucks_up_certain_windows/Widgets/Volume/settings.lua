volWidth       = easyTheme.barWidth
volHeight      = easyTheme.barWidth/2
volPBHeight    = easyTheme.barWidth/10
volBorderWidth = 0

volColor = {}
volColor.fg      = easyTheme.lightColor
volColor.bg      = easyTheme.darkColor
volColor.fg_mute = easyTheme.neutralColor
volColor.bg_mute = easyTheme.darkColor

toggle_mute_cmd   = "amixer sset Master toggle"
--toggle_mute_cmd   = "amixer -c 0 set Master playback toggle"
mute_cmd          = "amixer -c 0 set Master playback mute"
unmute_cmd        = "amixer -c 0 set Master playback unmute"
setDefaultVol_cmd = "amixer -c 0 set Master playback 0%"
setMaxVol_cmd     = "amixer -c 0 set Master playback 100%"
lowerVol_cmd      = "amixer sset Master 5%-"
raiseVol_cmd      = "amixer sset Master 5%+"
--lowerVol_cmd      = "amixer -c 0 set Master playback 1db-"
--raiseVol_cmd      = "amixer -c 0 set Master playback 1db+"

volTimeout = 2
