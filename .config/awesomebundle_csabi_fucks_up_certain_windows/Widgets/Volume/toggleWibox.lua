volToggleWibox = {}

for s = 1, screen.count() do
  volToggleWibox[s] = wibox({})
  volToggleWibox[s].height = 1
  volToggleWibox[s].width = screen[s].geometry.width/8
  volToggleWibox[s].visible = true
  volToggleWibox[s].ontop = true

  if showToggleWiboxes then
    volToggleWibox[s]:set_bg(toggleWiboxColor)
  end

  wiboxPositioning(volToggleWibox[s], s, 0, 0, "left", "bottom")

  volToggleWibox[s]:struts({top = 1})

  volToggleWibox[s]:connect_signal("mouse::enter", showVol)
end
