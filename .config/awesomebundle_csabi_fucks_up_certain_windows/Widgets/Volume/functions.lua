showVol = function()
  if volTimer.started then
    volTimer:stop()
  end
  volTimer:start()
  volTextboxWibox[mouse.screen.index].ontop = true
  for s=1,screen.count() do
    volTextboxWibox[s].visible = true
  end
  volPBWibox[mouse.screen.index].ontop      = true
end

removeVol = function()
  volTimer:stop()
  for s=1,screen.count() do
    volTextboxWibox[s].visible = false
    volTextboxWibox[s].ontop   = false
    volPBWibox[s].ontop        = false
  end
end

raiseVol = function()
  awful.util.spawn(raiseVol_cmd)
  vicious.force({volText,volPB})
  showVol()
end

lowerVol = function()
  awful.util.spawn(lowerVol_cmd)
  vicious.force({volText,volPB})
  showVol()
end

toggleVol = function()
  awful.util.spawn(toggle_mute_cmd) 
  vicious.force({volText,volPB})
  showVol()
end

leftButtonVol = function()
  toggleVol()
end

rightButtonVol = function()
  awful.util.spawn(mute_cmd)
  awful.util.spawn(setDefaultVol_cmd)
  vicious.force({volText,volPB})
  showVol()
end
