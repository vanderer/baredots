batPBWibox         = wibox({})
batPBWibox.width   = batPBWidth
batPBWibox.height  = screen[1].workarea.height
batPBWibox.visible = true

wiboxPositioning(batPBWibox, 1, 0, 0, "left", "top", "w")


--batPBWibox:struts({ left = batPBWibox.width })

batPBWibox:set_widget(batPB)

batPBWibox:connect_signal("mouse::enter", function() batWibox.ontop = true end)
batPBWibox:connect_signal("mouse::leave", function() batWibox.ontop = false end)

batWibox         = wibox({})
batWibox.width   = easyTheme.barWidth
batWibox.height  = 160
batWibox.visible = true


wiboxPositioning(batWibox, 1, 70, 50, "left", "top")
batWibox.y = batWibox.y + 100

local layout = wibox.layout.margin()
layout:set_margins(8)
layout:set_widget(batWidget)

batWibox:set_widget(layout)

batWibox.x=0
batWibox.y=screen[1].geometry.height*0.7-batWibox.height-2*batWibox.border_width
