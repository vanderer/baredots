dofile(widgetPath .. "settings.lua")
dofile(widgetPath .. "functions.lua")
dofile(widgetPath .. "progressbar.lua")
dofile(widgetPath .. "textbox.lua")
dofile(widgetPath .. "wibox.lua")
dofile(widgetPath .. "timer.lua")
--dofile(widgetPath .. "batFunction.lua")
dofile(widgetPath .. "keys.lua")

hadBat = true
batRemindTimerText = "Setting up..."

vicious.force({batPB})

checkBat()
