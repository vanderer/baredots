batPB = awful.widget.progressbar()
batPB:set_vertical(true)

batID = getBatID()
if batID == "" then batID = "BAT0" end

vicious.register(batPB, vicious.widgets.bat, batFunction, 3, batID)
