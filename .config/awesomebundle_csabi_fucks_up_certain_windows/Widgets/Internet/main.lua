dofile(widgetPath .. "settings.lua")
dofile(widgetPath .. "functions.lua")
dofile(widgetPath .. "textbox.lua")
dofile(widgetPath .. "wibox.lua")
dofile(widgetPath .. "timer.lua")
dofile(widgetPath .. "bindings.lua")

interfaces = getInterfaces()

haveInternet = false
hadInternet  = false

extIP = nil

internetFunction()
