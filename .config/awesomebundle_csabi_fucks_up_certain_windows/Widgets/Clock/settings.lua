clockWidth       = 400
clockHeigth      = 100
clockBorderWidth = 1
clockOffset      = 50

clockTimeout = 2

clockColor = {}
clockColor.fg     = easyTheme.lightColor
clockColor.bg     = easyTheme.darkColor
clockColor.border = easyTheme.lightColor
