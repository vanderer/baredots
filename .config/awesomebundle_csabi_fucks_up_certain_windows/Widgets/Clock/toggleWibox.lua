clockToggleWibox = {}

for s = 1, screen.count() do
  clockToggleWibox[s] = wibox({})
  clockToggleWibox[s].height = 1
  clockToggleWibox[s].width = screen[s].geometry.width/8
  clockToggleWibox[s].visible = true
  clockToggleWibox[s].ontop = true

  if showToggleWiboxes then
    clockToggleWibox[s]:set_bg(toggleWiboxColor)
  end

  wiboxPositioning(clockToggleWibox[s], s, 100, 0, "right", "top")
  clockToggleWibox[s].x = clockToggleWibox[s].x -1

  clockToggleWibox[s]:struts({top = 1})

  clockToggleWibox[s]:connect_signal("mouse::enter", showClock)
end
