--GENERATE THE MENU
csabiMenu = {}
csabiMenuWibox = {}
hiddenCsabiMenuWibox = {}
for s = 1,screen.count() do
  csabiMenu[s] = wibox.widget.textbox()
  csabiMenu[s]:set_text("Home")
  csabiMenu[s]:buttons(awful.util.table.join(
    awful.button({}, 1, function()
      makeEntryStaple(theDirectory,entryHeight,2, mouse.coords().x, mouse.coords().y)
      hideCsabiWibox()
    end)
  ))

  csabiMenuWibox[s] = wibox({})
  csabiMenuWibox[s].width = 100
  csabiMenuWibox[s].height = 100
  csabiMenuWibox[s].x = screen[s].geometry.x
  csabiMenuWibox[s].y = screen[s].geometry.y + screen[s].geometry.height
  csabiMenuWibox[s].visible = false
  csabiMenuWibox[s].ontop = true
  csabiMenuWibox[s]:set_bg("ffa50000")
  csabiMenuWibox[s]:set_widget(csabiMenu[s])

  hiddenCsabiMenuWibox[s] = wibox({})
  hiddenCsabiMenuWibox[s].width = 100
  hiddenCsabiMenuWibox[s].height = 1
  hiddenCsabiMenuWibox[s].x = screen[s].geometry.x
  hiddenCsabiMenuWibox[s].y = screen[s].geometry.y + screen[s].geometry.height - 1
  hiddenCsabiMenuWibox[s].visible = true
  hiddenCsabiMenuWibox[s].ontop = true
  hiddenCsabiMenuWibox[s]:set_bg("ffa50000")

  showCsabiWibox = function()
    hiddenCsabiMenuWibox[mouse.screen].visible = false
    csabiMenuWibox[mouse.screen].visible = true
  end

  hideCsabiWibox = function()
    hiddenCsabiMenuWibox[mouse.screen].visible = true
    csabiMenuWibox[mouse.screen].visible = false
  end

  hiddenCsabiMenuWibox[s]:connect_signal("mouse::enter", function()
    local size = 0
    for _ in pairs(globalWiboxContainer) do size = size + 1 end
    if size > 0 then
      closeCsabiMenu()
    else
      keygrabber.run(shouldWeContinue)
      makeEntryStaple(getDirectoryEntries(globalStartingPoint),entryHeight,2, mouse.coords().x+20, mouse.coords().y)
    end
  end)
  csabiMenuWibox[s]:connect_signal("mouse::leave", hideCsabiWibox)

end
