-- Pacman Widget
pacWibox = {}
pacwidget = wibox.widget.textbox()

pacwidgetTT = nil

vicious.register(pacwidget, vicious.widgets.pkg,
                function(widget,args)
                    local io = { popen = io.popen }
                    local s = io.popen("pacman -Qu")
                    local str = ''

                    for line in s:lines() do
                        str = str .. line .. "\n"
                    end
                    pacwidgetTT = str
                    s:close()
                    return "  UPDATES: " .. args[1] .. "  "
                end, 300, "Arch")
pacwidget:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.util.spawn_with_shell("urxvt" .. " -e " .. "yaourt -Syu") end)
                           ))

showPacTT = function()
  pacTT = naughty.notify({ title     = "Updates",
                                text      = pacwidgetTT,
                                --icon      = ico_path .. "pacman.png",
                                --icon_size = 42,
                                preset    = naughty.config.presets.tooltip,
                              })
end
deletePacTT = function()
  naughty.destroy(pacTT)
end

pacwidget:add_signal("mouse::enter", showPacTT)
pacwidget:add_signal("mouse::leave", deletePacTT)
for s=1,screen.count() do
  pacWibox[s] = wibox({})
  pacWibox[s].width = 125
  pacWibox[s].height = 30
  pacWibox[s].ontop = true
  pacWibox[s].visible = true
  pacWibox[s]:set_bg("#333333")
  pacWibox[s]:set_fg("888888")
  pacWibox[s]:set_widget(pacwidget)
  pacWibox[s].x = 300
  pacWibox[s].y = 0
end
