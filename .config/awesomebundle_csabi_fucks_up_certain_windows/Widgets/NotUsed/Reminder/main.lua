reminderWidgetPath = widgetPath .. ""
reminder_file = reminderWidgetPath .. "todo.csv"
projects_file = reminderWidgetPath .. "projects.csv"
------------------------------------
arrangeTodos = function(csv)
  toshow = {}
  for line in io.lines(csv) do
    if line == "" then
      table.insert(toshow, line)
    else
      typ = string.sub(line,3,3)
      if typ ~= "." then
        if typ=="-" then
        else
          table.insert(toshow, line)
        end
      else
        day   = string.sub(line,1, 2) + 0
        month = string.sub(line,4, 5) + 0
        year  = string.sub(line,7,10) + 0
        today = os.date("*t")
        if today.year == year then
          if month - today.month < 2 then
            table.insert(toshow, string.sub(line,12) .. "  ( " .. string.sub(line,1,10) .. " )")
          end
        elseif today.year + 1 == year then
          if today.month - month > 10 then
            table.insert(toshow, string.sub(line,12) .. "  ( " .. string.sub(line,1,10) .. " )")
          end
        end
      end
    end
  end
  return toshow
end
------------------------------------
wiegebichaus = function(tabelle)
  res = ""
  for i = 1, table.maxn(tabelle) do
    if tabelle[i] == "" then
      res = res .. "\n"
    else
      res = res .. tabelle[i] .. "\n"
    end
  end
  res = res:gsub("\n$","")
  return res
end
------------------------------------
csabiSchreiMichAn = function()
  tos = arrangeTodos(reminder_file)

  ausgabe = wiegebichaus(tos)

  naughty.destroy(naugtyreminder) 
  if ausgabe == "" then
  else
    naugtyreminder = naughty.notify({ text= ausgabe,
                     title="Vergiss nicht...",
                     timeout=0,
                     bg=beautiful.bg_urgent,
                     fg=beautiful.fg_urgent,
                     position="top_right"})
  end
end
------------------------------------
------------------------------------
arrangeProjects = function(csv)
  toshow = {}
  for line in io.lines(csv) do
    if line == "" then
      table.insert(toshow, line)
    else
      typ = string.sub(line,2,2)
      if typ == "-" then
      else
        table.insert(toshow, string.sub(line,3))
      end
    end
  end
  return toshow
end
------------------------------------
csabiProjekte = function()
  tos = arrangeProjects(projects_file)

  ausgabe = wiegebichaus(tos)

  naughty.destroy(naugtyprojects) 
  naugtyprojects = naughty.notify({ text= ausgabe,
                   title="Du könntest...",
                   preset = naughty.config.presets.tooltip,
                   --bg=beautiful.bg_focus,
                   --fg=beautiful.fg_focus,
                   position="top_right"})
end
------------------------------------
globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey            }, "F3", function () csabiSchreiMichAn() end))
globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey            }, "F4", function () naughty.destroy(naugtyreminder) end))
globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey            }, "a", function () awful.util.spawn( editor .. " " .. reminder_file ) end))

globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey            }, "F5", function () csabiProjekte() end))
globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey            }, "F6", function () naughty.destroy(naugtyprojects) end))
globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey            }, "o", function () awful.util.spawn( editor .. projects_file ) end))
------------------------------------
todoTimer = timer({ timeout = 3600 })
todoTimer:connect_signal("timeout", csabiSchreiMichAn)
todoTimer:start()
