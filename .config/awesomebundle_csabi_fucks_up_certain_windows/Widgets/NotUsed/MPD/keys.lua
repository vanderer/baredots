globalkeys = awful.util.table.join(globalkeys,
  awful.key({                   }, "XF86AudioPlay", mpdToggle),
  awful.key({                   }, "XF86AudioPrev", mpdPrev),
  awful.key({                   }, "XF86AudioNext", mpdNext)
)
