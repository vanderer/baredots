windowsTitleWibox = {}
windowsHiddenTitleWibox = {}
for s=1,screen.count() do
  windowsTitleWibox[s] = wibox({})
  windowsTitleWibox[s].width = windowsTitleWidth
  windowsTitleWibox[s].height = windowsTitleHeight
  windowsTitleWibox[s].visible = true
  windowsTitleWibox[s].ontop = false
  windowsTitleWibox[s]:set_bg(windowsTitleColor.bg)

  windowsTitleWibox[s].x = screen[s].geometry.x
  windowsTitleWibox[s].y = screen[s].geometry.y

  awful.placement.centered(windowsTitleWibox[s])
  windowsTitleWibox[s].y = screen[s].geometry.y

  windowsTitleWibox[s]:set_widget(windowsTitleLayout)
end
