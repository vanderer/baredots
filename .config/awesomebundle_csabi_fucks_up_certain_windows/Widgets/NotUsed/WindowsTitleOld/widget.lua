windowsTitleTextbox = wibox.widget.textbox()
windowsIcon = wibox.widget.imagebox()

windowsTitleTextbox:set_text("")
windowsTitleTextbox:set_align("left")

windowsTitle = wibox.widget.background()
windowsTitle:set_widget(windowsTitleTextbox)
windowsTitle:set_fg(windowsTitleColor.fg)

windowsTitleMarginLayout = wibox.layout.margin()
windowsTitleMarginLayout:set_left(8)
windowsTitleMarginLayout:set_widget(windowsTitle)

windowsTitleLayout = wibox.layout.fixed.horizontal()
windowsTitleLayout:add(windowsIcon)
windowsTitleLayout:add(windowsTitleMarginLayout)


client.connect_signal("property::icon", function() windowsIcon:set_image(client.focus.icon) end)
client.connect_signal("focus", function() windowsTitleTextbox:set_text(client.focus.name)
                                          windowsIcon:set_image(client.focus.icon)
                                        end)
client.connect_signal("property::name", function() windowsTitleTextbox:set_text(client.focus.name) end)
client.connect_signal("unfocus", function() windowsTitleTextbox:set_text("")
                                            windowsIcon:set_image()
                                          end)
