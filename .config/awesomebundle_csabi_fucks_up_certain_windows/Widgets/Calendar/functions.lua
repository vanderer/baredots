function getWeekNumber()
  awful.util.pread("date '+%W'")
end

function getDayOfWeek()
  awful.util.pread("date '+%w'")
end
