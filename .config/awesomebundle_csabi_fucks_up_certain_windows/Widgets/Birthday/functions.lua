--returns if the year is leap
isLeap = function(year)
  if math.fmod(year, 400) == 0 then
    return true
  elseif math.fmod(year, 100) == 0 then
    return false
  elseif math.fmod(year, 4) == 0 then
    return true
  else
    return false
  end
end


getBirthdays = function(bd_file)
  bdays = {}  -- this will be the list of birthdays that are in sight
  today = os.date("*t") -- today = {year, month, day,...} 
  for line in io.lines(bd_file) do --run through the birthday file line by line
    if string.sub(line,1,2) ~= "--" then --retain the ability to have comments in bd_file (lines beginning with -- but not with <SPACE>--)
      name         = string.sub(line,12)  --get the name of the person who has his birthday (first 11 characters are reserved for date)

      --next save the coresponding config-entries to their variables (we treat line by line which means birthday by birthday)
      birthday     = os.date("*t", os.time({ month = tonumber(string.sub(line,1,2)),
                                             day   = tonumber(string.sub(line,4,5)),
                                             year  = tonumber(string.sub(line,7,10)),
                                           }))
      
      --create the string which we will display in the birthday notification to represent the birthday date
      birthdayStr = birthday.day .. "." .. birthday.month .. "." .. birthday.year

      --check if the birthday has passed already this year
      hasPassed = false
      if birthday.month < today.month then
        hasPassed = true
      elseif birthday.month == today.month and birthday.day < today.day then
        hasPassed = true
      end


      --start out with what you assume to be the next birthday of the current line
      nextBirthday = {day = birthday.day, month = birthday.month, year = today.year} -- month is correct!!!
      if hasPassed then                           -- if it has passed this year already
        nextBirthday.year = nextBirthday.year + 1 -- then the next birthday is next year
      end -- year is correct!!!
      if nextBirthday.month == 2 and nextBirthday.day == 29 and (not isLeap(nextBirthday.year)) then
        nextBirthday.day = 28  --here we help all those poor people who have their birthday on Feb.29 :)
      end -- day is correct!!!

      nextBirthday = os.date("*t", os.time(nextBirthday)) --here we turn our simple table into a lua-date-table

      nextBirthdayStr = nextBirthday.day .. "." .. nextBirthday.month .. "." .. nextBirthday.year


      --here we get how many days we have to wait for the next occurence of this lines birthday
      if hasPassed then
        if isLeap(today.year) then
          howFar = 366 - today.yday + nextBirthday.yday --here we can use that lua-date-tables are cool :) yday = which day of the year ;)
        else
          howFar = 365 - today.yday + nextBirthday.yday
        end
      else
        howFar = nextBirthday.yday - today.yday
      end


      becomes = nextBirthday.year - birthday.year -- determine next age of this lines person

      table.insert(bdays, { name            = name, 
                            birthday        = birthday,
                            birthdayStr     = birthdayStr,
                            nextBirthday    = nextBirthday, 
                            nextBirthdayStr = nextBirthdayStr,
                            howFar          = howFar,
                            becomes         = becomes,
                          })
    end
  end
  sortFunction = function(elem1, elem2)
    return (elem1.howFar < elem2.howFar)
  end
  table.sort(bdays, sortFunction)
  return bdays -- of the form: { {name1,birthdays,bStr,...},{name2,birthdays,bStr,...}, ..... } 
end


--run through a table of birthdays and return those which are in a range within offsetTime
getNextBirthdays = function(bdays, offsetTime)
  nBDays = {}
  offsetTime = offsetTime or 31
  for i = 1, table.maxn(bdays) do
    if bdays[i].howFar <= offsetTime then
      table.insert(nBDays,bdays[i])
    end
  end
  return nBDays
end


--prepare one birthday -- what to display and color by ungency
bdayToString = function(bday)
  if bday.howFar < 6 then
    col = 'red'
  elseif bday.howFar < 11 then
    col = 'orange'
  else
    col = 'green'
  end
  res = bday.name .. " <span color='" .. col .. "'>[" .. bday.howFar .. "] (" .. bday.nextBirthdayStr .. ") {" .. bday.becomes .. "}</span>"
  return res
end


-- work on a list of birthdays
bdaysToString = function(bdays)
  res = ""
  maxIdx = table.maxn(bdays)
  if maxIdx ~= 0 then
    res = bdayToString(bdays[1])
    for i = 2, maxIdx do
      res = res .. "\n" .. bdayToString(bdays[i]) 
    end
  end
  return res
end


--create the widget
setBirthdayWidgets = function()
  birthdays = getBirthdays(birthdayPath)
  nextBirthdays = getNextBirthdays(birthdays)
  if table.maxn(nextBirthdays) ~= 0 and nextBirthdays[1].howFar == 0 then --light up if we have a birthday today
    birthdayImage:set_image(gears.surface(birthdayIcons.today))
    birthdayTTIcon = birthdayIcons.tooltipToday
  else                                                                    -- otherwise just show the grey image
    birthdayImage:set_image(gears.surface(birthdayIcons.notToday))
    birthdayTTIcon = birthdayIcons.tooltipNotToday
  end
  birthdayWidgetText:set_text(birthdayIconText .. table.maxn(nextBirthdays)) -- do the count of nearby birthdays
  birthdayTTText = bdaysToString(nextBirthdays) -- create tooltip with all the nice information
  if birthdayTTText == "" then
    birthdayTTText = "No birthday in sight"
  end
end


--the tooltip itself
showBirthdayTT = function()
  birthdayTT = naughty.notify({ 
                                screen    = mouse.screen,
                                title     = "Birthdays",
                                text      = birthdayTTText,
                                icon      = birthdayTTIcon,
                                icon_size = 42,
                                preset    = naughty.config.presets.tooltip,
                              })
end

deleteBirthdayTT = function()
  naughty.destroy(birthdayTT)
end

