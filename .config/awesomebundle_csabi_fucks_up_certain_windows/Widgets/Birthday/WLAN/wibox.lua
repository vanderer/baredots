wlanWibox = {}

for s = 1, screen.count() do
  wlanWibox[s] = wibox({})
  wlanWibox[s].height = 100
  wlanWibox[s].width = 250
  wlanWibox[s].visible = true

  wiboxPositioning(wlanWibox[s], s, 70, 50, "left", "top")

  local layout = wibox.layout.margin()
  layout:set_margins(8)
  layout:set_widget(wlanWidget)

  wlanWibox[s]:set_widget(layout)
end

