-- csabi, awesome3 theme

--{{{ Main
local awful = require("awful")
awful.util = require("awful.util")

theme = {}

home          = os.getenv("HOME")
config        = awful.util.getdir("config")
shared        = "/usr/share/awesome"
if not awful.util.file_readable(shared .. "/icons/awesome16.png") then
    shared    = "/usr/share/local/awesome"
end
sharedicons   = shared .. "/icons"
sharedthemes  = shared .. "/themes"
themes        = config .. "/themes"
themename     = "/csabi"
if not awful.util.file_readable(themes .. themename .. "/theme.lua") then
       themes = sharedthemes
end
themedir      = themes .. themename

wallpaper1    = themedir .. "/background.jpg"
wallpaper2    = themedir .. "/background.png"
wallpaper3    = sharedthemes .. "/zenburn/zenburn-background.png"
wallpaper4    = sharedthemes .. "/default/background.png"
wpscript      = home .. "/.wallpaper"

if awful.util.file_readable(wallpaper1) then
  theme.wallpaper = wallpaper1
elseif awful.util.file_readable(wallpaper2) then
  theme.wallpaper = wallpaper2
elseif awful.util.file_readable(wpscript) then
  theme.wallpaper_cmd = { "sh " .. wpscript }
elseif awful.util.file_readable(wallpaper3) then
  theme.wallpaper = wallpaper3
else
  theme.wallpaper = wallpaper4
end
--}}}

theme.font          = "MarkWeb-Light 12"

theme.bg_normal     = easyTheme.darkColor
theme.bg_focus      = easyTheme.darkColor
theme.bg_urgent     = easyTheme.lightColor
theme.bg_minimize   = "#000000"

theme.fg_normal     = easyTheme.lightColor
theme.fg_focus      = easyTheme.lightColor
theme.fg_urgent     = easyTheme.darkColor
theme.fg_minimize   = "#1793d1"

--theme.useless_gap = 15
theme.border_width  = "0"
theme.border_normal = easyTheme.darkColor
theme.border_focus  = easyTheme.borderColor
theme.border_marked = easyTheme.borderColor

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
--theme.taglist_bg_focus = "#ff0000"
theme.titlebar_bg = easyTheme.darkColor
if string.len(easyTheme.darkColor) > 7 then
theme.titlebar_bg = string.sub(theme.titlebar_bg,1,7)
end
--theme.titlebar_bg_focus = easyTheme.lightColor
--if string.len(easyTheme.darkColor) > 7 then
--theme.titlebar_bg_focus = string.sub(theme.titlebar_bg,1,7)
--end

-- Display the taglist squares
theme.taglist_squares_sel   = sharedthemes .. "/default/taglist/squarefw.png"
theme.taglist_squares_unsel = sharedthemes .. "/default/taglist/squarew.png"

theme.tasklist_floating_icon = sharedthemes .. "/default/tasklist/floatingw.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = sharedthemes .. "/default/submenu.png"
theme.menu_height = "15"
theme.menu_width  = "100"

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
titlebarPath = config .. "/Themes/csabi/titlebar_redhalo/" 
theme.titlebar_close_button_normal =  titlebarPath .. "close_normal.png"
theme.titlebar_close_button_focus  =  titlebarPath .. "close_focus.png"

theme.titlebar_ontop_button_normal_inactive =  titlebarPath .. "ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  =  titlebarPath .. "ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active =  titlebarPath .. "ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  =  titlebarPath .. "ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive =  titlebarPath .. "sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  =  titlebarPath .. "sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active =  titlebarPath .. "sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  =  titlebarPath .. "sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive =  titlebarPath .. "floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  =  titlebarPath .. "floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active =  titlebarPath .. "floating_normal_active.png"
theme.titlebar_floating_button_focus_active  =  titlebarPath .. "floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive =  titlebarPath .. "maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  =  titlebarPath .. "maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active =  titlebarPath .. "maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  =  titlebarPath .. "maximized_focus_active.png"

-- Colored Theme Buttons
theme.layout_fairh = sharedthemes .. "/default/layouts/fairh.png"
theme.layout_fairv = sharedthemes .. "/default/layouts/fairv.png"
theme.layout_floating  = sharedthemes .. "/default/layouts/floating.png"
theme.layout_magnifier = sharedthemes .. "/default/layouts/magnifier.png"
theme.layout_max = sharedthemes .. "/default/layouts/max.png"
theme.layout_fullscreen = sharedthemes .. "/default/layouts/fullscreen.png"
theme.layout_tilebottom = sharedthemes .. "/default/layouts/tilebottom.png"
theme.layout_tileleft   = sharedthemes .. "/default/layouts/tileleft.png"
theme.layout_tile = sharedthemes .. "/default/layouts/tile.png"
theme.layout_tiletop = sharedthemes .. "/default/layouts/tiletop.png"
theme.layout_spiral  = sharedthemes .. "/default/layouts/spiral.png"
theme.layout_dwindle = sharedthemes .. "/default/layouts/dwindle.png"
-- You can use your own layout icons like this:
-- theme.layout_fairh = sharedthemes .. "/default/layouts/fairhw.png"
-- theme.layout_fairv = sharedthemes .. "/default/layouts/fairvw.png"
-- theme.layout_floating  = sharedthemes .. "/default/layouts/floatingw.png"
-- theme.layout_magnifier = sharedthemes .. "/default/layouts/magnifierw.png"
-- theme.layout_max = sharedthemes .. "/default/layouts/maxw.png"
-- theme.layout_fullscreen = sharedthemes .. "/default/layouts/fullscreenw.png"
-- theme.layout_tilebottom = sharedthemes .. "/default/layouts/tilebottomw.png"
-- theme.layout_tileleft   = sharedthemes .. "/default/layouts/tileleftw.png"
-- theme.layout_tile = sharedthemes .. "/default/layouts/tilew.png"
-- theme.layout_tiletop = sharedthemes .. "/default/layouts/tiletopw.png"
-- theme.layout_spiral  = sharedthemes .. "/default/layouts/spiralw.png"
-- theme.layout_dwindle = sharedthemes .. "/default/layouts/dwindlew.png"

theme.awesome_icon = themedir .. "/awesome16.png"

theme.tooltip_border_color = easyTheme.borderColor
theme.tooltip_fg_color = easyTheme.fgColor
theme.tooltip_bg_color = easyTheme.bgColor

theme.notify_border_color = easyTheme.borderColor
theme.notify_fg = easyTheme.fgColor
theme.notify_bg = easyTheme.bgColor

return theme
--vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:encoding=utf-8:textwidth=80
