naughty.config.presets.normal.border_color  = beautiful.tooltip_border_color
naughty.config.presets.normal.border_width  = 2
naughty.config.presets.normal.fg            = beautiful.notify_fg
naughty.config.presets.normal.bg            = beautiful.notify_bg
naughty.config.presets.normal.margin        = 20

naughty.config.presets.critical.timeout     = 5
naughty.config.presets.critical.margin      = 20

naughty.config.presets.tooltip = {}
naughty.config.presets.tooltip.position     = "top_left"
naughty.config.presets.tooltip.timeout      = 5
naughty.config.presets.tooltip.margin       = 20
naughty.config.presets.tooltip.border_color = beautiful.tooltip_border_color
naughty.config.presets.tooltip.fg           = beautiful.tooltip_fg_color
naughty.config.presets.tooltip.bg           = beautiful.tooltip_bg_color
naughty.config.presets.tooltip.border_width = 5
