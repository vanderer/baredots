doWidget("Run", 1)
doWidget("Battery", 2)
doWidget("WLAN", 5)
--doWidget("CsabiMenu", 5)
doWidget("Internet", 3)
doWidget("Volume", 1)
doWidget("Birthday", 1)
--doWidget("Vicious", 4)
--doWidget("NewMenu", 5)
doWidget("TaglistVertical", 1)
doWidget("Tray", 1)
doWidget("Xthings", 1)
doWidget("Textclock", 1)
doWidget("TextDate", 2)
--doWidget("Calendar", 5)

for s=1,screen.count() do
  if awesomeSettings.awesomeness > 1 then
    widgetGroupPlacement({myClockWibox[s], myDateWibox[s]} , 0, 600, 5, screen[s])
  end
end

for s=1,screen.count() do
  if awesomeSettings.awesomeness > 2 then
    if wlanWibox ~=nil then
      widgetGroupPlacement({internetWibox[s], wlanWibox[s], myBirthdayWibox[s]} , 65, 0, 5, screen[s])
    elseif internetWibox ~=nil then
      widgetGroupPlacement({internetWibox[s], myBirthdayWibox[s]} , 65, 0, 5, screen[s])
    else
      widgetGroupPlacement({myBirthdayWibox[s]} , 65, 0, 5, screen[s])
    end
  else
    widgetGroupPlacement({myBirthdayWibox[s]} , 65, 0, 5, screen[s])
  end
end
