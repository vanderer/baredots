-- PATH VARIABLES -- MUST BE PROCESSED HERE --
  -- Standard awesome library
  gears         = require("gears")
  awful         = require("awful")
  awful.rules   = require("awful.rules")
  require("awful.autofocus")
  wibox         = require("wibox")
  beautiful     = require("beautiful")
  naughty       = require("naughty")
  menubar       = require("menubar")
  vicious       = require("vicious")
-- PATH VARIABLES -- MUST BE PROCESSED HERE --

awesome_root = awful.util.getdir("config") .. "/"

awesomeSettings = {}
awesomeSettings.awesomeness=10


dofile(awesome_root .. "functions.lua")
dofile(awesome_root .. "easyTheme.lua")
dofile(awesome_root .. "errorHandling.lua")
dofile(awesome_root .. "variables.lua")
dofile(awesome_root .. "layouts.lua")
dofile(awesome_root .. "wallpaper.lua")
dofile(awesome_root .. "tags.lua")
dofile(awesome_root .. "widgets.lua")
dofile(awesome_root .. "mouseBindings.lua")
dofile(awesome_root .. "keyBindings.lua")
dofile(awesome_root .. "rules.lua")
dofile(awesome_root .. "signals.lua")
