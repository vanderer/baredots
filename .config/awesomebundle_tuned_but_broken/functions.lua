function getComputerProperties()
  local path = awful.util.pread("echo $HOME")
  path = string.sub(path, 1, string.len(path)-1) .. "/.awesomeSettings.lua"
  if type(awful.util.checkfile(path)) == "function" then
    dofile(path)
  end
end

function doWidget(name, awesomeness)
  if not awesomeness then awesomeness = 10 end
  if awesomeness <= awesomeSettings.awesomeness then
    widgetPath = awesome_root .. "Widgets/" .. name .. "/"
    dofile(widgetPath .. "main.lua")
  end
end

function doExtended(name)
  if not awesomeness then awesomeness = 10 end
  if awesomeness <= awesomeSettings.awesomeness then
    extendedPath = awesome_root .. "Extended/" .. name .. "/"
    dofile(extendedPath .. "main.lua")
  end
end

function doBase(name)
  dofile(awesome_root .. "Base/" .. name .. ".lua")
end

function tooltip(widget)

end

function startProg(str, opts)
  if awful.util.pread("ps -e | grep " .. str) == "" then
    awful.util.spawn(str .. " " .. opts)
  end
end

function stopProg(str)
  awful.util.spawn("killall " .. str)
end

function startFileManager(path)
  awful.util.spawn(filemanager .. " " .. path)
end

-- wibox: Wibox
-- s: screen
-- xQuotient: number between 0 and 100 (percentage of the screen from top left corner) default: 0
-- yQuotient: number between 0 and 100 (percentage of the screen from top left corner) default: 0
-- horizontallyAt: "left", "center", "right" (aligns the left, center, right of the wibox with the given point) default: left
-- verticallyAt: "top", "center", "bottom" (aligns the top, center, bottom of the wibox with the given point) default: top
-- mode: "g" or "w" (uses the geometry or the workarea on the given screen) default: geometry
-- ignore(...)Border: true or false (shall the (...) border be considered or not) default: false
function wiboxPositioning(wibox, s, xQuotient, yQuotient, horizontallyAt, verticallyAt, mode, ignoreHorizontalBorder, ignoreVerticalBorder)
  if not xQuotient              then xQuotient              = 0        end
  if not yQuotient              then yQuotient              = 0        end
  if not horizontallyAt         then horizontallyAt         = "center" end
  if not verticallyAt           then verticallyAt           = "center" end
  if not mode                   then mode                   = "g"      end
  if not ignoreHorizontalBorder then ignoreHorizontalBorder = false    end
  if not ignoreVerticalBorder   then ignoreVerticalBorder   = false    end

  xQuotient = xQuotient/100
  yQuotient = yQuotient/100
 
  local table = screen[s].geometry
  if mode == "w" then
    table = screen[s].workarea
  end
 
  wibox.x = table.x + xQuotient * table.width
  wibox.y = table.y + yQuotient * table.height
  
  if horizontallyAt == "left" then
    if ignoreVerticalBorder then
      wibox.x = wibox.x - wibox.border_width
    end
  elseif horizontallyAt == "center" then
    wibox.x = wibox.x - wibox.width/2 - wibox.border_width
  elseif horizontallyAt == "right" then
    wibox.x = wibox.x - wibox.width - wibox.border_width
    if not ignoreVerticalBorder then
      wibox.x = wibox.x - wibox.border_width
    end
  else
    naughty.notify({ screen    = s,
                     title     = "Placement Error",
                     text      = "'" .. horizontallyAt .. "' is not a valid horizontal alignment!",
                     preset    = naughty.config.presets.critical,
                  })
  end

  if verticallyAt == "top" then
    if ignoreHorizontalBorder then
      wibox.y = wibox.y - wibox.border_width
    end
  elseif verticallyAt == "center" then
    wibox.y = wibox.y - wibox.height/2 - wibox.border_width
  elseif verticallyAt == "bottom" then
    wibox.y = wibox.y - wibox.height - wibox.border_width
    if not ignoreHorizontalBorder then
      wibox.y = wibox.y - wibox.border_width
    end
  else
    naughty.notify({ screen    = s,
                     title     = "Placement Error",
                     text      = "'" .. verticallyAt .. "' is not a valid vertical alignment!",
                     preset    = naughty.config.presets.critical,
                  })
  end
end

function countAll(table)
  local count = 0
  for _,_ in pairs(table) do 
    count = count + 1
  end
  return count
end

function bumpDone()
    naughty.notify({ screen    = mouse.screen,
                     title     = "This script has been executed",
                     text      = "... yes it has!",
                     preset    = naughty.config.presets.critical,
                  })
end

function widgetGroupPlacement(wiboxesTable, x, y, margin, s)
  currentXPosition = s.geometry.x + x
  currentYPosition = s.geometry.y + y

  for i,value in ipairs(wiboxesTable) do
    if currentYPosition + value.height > s.geometry.height then
      currentYPosition = s.geometry.y + y
      currentXPosition = currentXPosition + value.width + margin 
    end
    value.x = currentXPosition
    value.y = currentYPosition
    currentYPosition = currentYPosition + value.height + margin
  end
end
