dofile(widgetPath .. "settings.lua")
dofile(widgetPath .. "function.lua")
dofile(widgetPath .. "textboxes.lua")
dofile(widgetPath .. "wibox.lua")

-------------------------------------------------------------

launchPrgs =  { 
  browser, 
  terminal, 
  "marlin", 
  "mpd",
  "transmission-gtk",
} 

launchText = {
  "Browser",
  "Terminal",
  "Files",
  "MPD",
  "Transmission",
}

launcherWidget("Programs", launchText, launchPrgs, 3/6)

-------------------------------------------------------------

systemPrgs = { 
  poweroff_cmd,
  reboot_cmd,
  suspend_cmd,
  pacman_update_cmd,
}

systemText = {
  "Poweroff",
  "Reboot",
  "Suspend",
  "Update System",
}

launcherWidget("System", systemText, systemPrgs, 1/6)

-------------------------------------------------------------

mountPrgs = { 
  "pmount /dev/mmcblk0p1 sdkarte",
  "pmount /dev/sdb1 usbstick",
  "marlin /media/sdkarte",
  "marlin /media/usbstick",
}

mountText = {
  "mount Sdcard",
  "mount Usbstick",
  "Sdcard",
  "Usbstick",
}

launcherWidget("Mount", mountText, mountPrgs, 5/6)

-------------------------------------------------------------

awesomePrgs = { 
  awesome.quit,
  awesome.restart,
}

awesomeText = {
  "Quit Awesome",
  "Reload Awesome",
}

launcherWidget("Awesome", awesomeText, awesomePrgs, 2/6)

-------------------------------------------------------------

keyboardLayoutText = {
  "Colemak",
  "Dvorak",
  "DVP",
  "CH",
  "US",
}

keyboardLayoutPrgs = {
  function() setKeyboardLayout('us(colemak)') end,
  function() setKeyboardLayout('us(dvorak)') end,
  function() setKeyboardLayout('us(dvp)') end,
  function() setKeyboardLayout('ch') end,
  function() setKeyboardLayout('us') end,
}

keyboardLayoutWiboxes = launcherWidget("Keyboard Layout", keyboardLayoutText, keyboardLayoutPrgs, 4/6)

setKeyboardLayout('us(dvp)')
-------------------------------------------------------------

