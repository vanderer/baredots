launcherColor = {}
launcherColor.fg       = easyTheme.neutralColor
launcherColor.bg       = "#050505"
launcherColor.bg_focus = "#111111"
launcherColor.title_fg = easyTheme.darkColor
launcherColor.title_bg = easyTheme.neutralColor
