windowsTitleWibox = {}

for s=1,screen.count() do
  windowsTitleWibox[s] = wibox({})
  windowsTitleWibox[s].width = windowsTitleWidth
  windowsTitleWibox[s].height = windowsTitleHeight
  windowsTitleWibox[s].visible = true
  windowsTitleWibox[s].ontop = false
  windowsTitleWibox[s]:set_bg(windowsTitleColor.bg)

  wiboxPositioning(windowsTitleWibox[s], s, 50, 0, "center", "top")

  local windowsTitleMarginLayout = wibox.layout.margin()
  if showWindowsTitleIcon then
    windowsTitleMarginLayout:set_left(8)
  end
  windowsTitleMarginLayout:set_widget(windowsTitle[s].background)
  
  local windowsTitleLayout = wibox.layout.fixed.horizontal()
  if showWindowsTitleIcon then
    windowsTitleLayout:add(windowsTitle[s].icon)
  end
  windowsTitleLayout:add(windowsTitleMarginLayout)

  windowsTitleWibox[s]:set_widget(windowsTitleLayout)
end
