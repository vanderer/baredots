dofile(widgetPath .. "settings.lua")
globalWiboxContainer = {}

globalStartingPoint = awful.util.pread("echo $HOME")
globalStartingPoint = string.sub(globalStartingPoint,1,string.len(globalStartingPoint)-1) .. "/"

direction = 1

dofile(widgetPath .. "helperFunctions.lua")

createOneMenuEntry = function(name, x_coord, y_coord, width, height, entry_level)
  --CREATE TEXTWIDGET
  thisEntryTextWidget = wibox.widget.textbox()
  thisEntryTextWidget:set_markup(makeNiceName(name))
  thisEntryTextWidget:buttons(awful.util.table.join(
  awful.button({}, 1, function()
    if makeNiceName(name) == "  ." then
      closeCsabiMenu()
      awful.util.spawn_with_shell("xdg-open '" .. name .. "'")
    elseif isDir(name) then
      --FIRST DELETE ALL HIGHER LEVEL WIBOXES
      for i,value in ipairs(globalWiboxContainer) do
        if value.level > entry_level then
          value.content.screen = nil
          value.content.visible = false
          value.content.ontop = false
          value.content = nil
          globalWiboxContainer[i] = nil --THIS IS VERY IMPORTANT !! THE PREVIOUS COMMANDS DON'T DELETE THE ENTRY
        end
      end

      for i,value in ipairs(globalWiboxContainer) do
        if value.level == entry_level then
          value.content:set_bg("#222222")
        end
      end
      if isDir(name) then
        if (x_coord + direction*2*width > screen[mouse.screen].geometry.x + screen[mouse.screen].geometry.width) or (x_coord + direction*2*width < screen[mouse.screen].geometry.x) then
          direction = direction * (-1)
        end
        makeEntryStaple(getDirectoryEntries(name),entryHeight,entry_level, x_coord + direction * width, y_coord)
      end
    else
      closeCsabiMenu()
      awful.util.spawn_with_shell("xdg-open '" .. name .. "'")
    end
  end)
  ))

  --CREATE AND POSITION WIBOXES
  local thisEntryWibox = wibox({})
  thisEntryWibox.width = width
  thisEntryWibox.height = height
  thisEntryWibox.visible = true
  thisEntryWibox.ontop = true
  thisEntryWibox.y = y_coord - height
  thisEntryWibox.x = x_coord
  thisEntryWibox.border_width = 2
  thisEntryWibox.border_color =  "#008656"
  thisEntryWibox.level = entry_level
  thisEntryWibox:set_bg("#222222")
  thisEntryWibox:set_widget(thisEntryTextWidget)

  --ADD THE NEXT LEVEL OF ENTRY STAPLE
  --but first color lower levels dark
  for i,value in ipairs(globalWiboxContainer) do
    if value.level < entry_level then
      value.content:set_bg("#000000")
    end
  end

  --thisEntryWibox:connect_signal("mouse::enter", function()
    ----FIRST DELETE ALL HIGHER LEVEL WIBOXES
    --for i,value in ipairs(globalWiboxContainer) do
      --if value.level > entry_level then
        --value.content.screen = nil
        --value.content.visible = false
        --value.content.ontop = false
        --value.content = nil
        --globalWiboxContainer[i] = nil --THIS IS VERY IMPORTANT !! THE PREVIOUS COMMANDS DON'T DELETE THE ENTRY
      --end
    --end

    --for i,value in ipairs(globalWiboxContainer) do
      --if value.level == entry_level then
        --value.content:set_bg("#222222")
      --end
    --end
    --if isDir(name) then
      --if (x_coord + direction*2*width > screen[mouse.screen].geometry.x + screen[mouse.screen].geometry.width) or (x_coord + direction*2*width < screen[mouse.screen].geometry.x) then
        --direction = direction * (-1)
      --end
      --makeEntryStaple(getDirectoryEntries(name),entryHeight,entry_level, x_coord + direction * width, y_coord)
    --end
  --end)

  --BUNDLE IT TO PAIRS OF {WIBOX, LEVEL}
  thisEntryPair = {}
  thisEntryPair.content = thisEntryWibox
  thisEntryPair.level = entry_level
  thisEntryWibox = nil
  return thisEntryPair
end

makeEntryStaple = function(list, height, staple_level, startingPoint_x, startingPoint_y)
  --GET SIZE OF LIST
  count = 0
  for _ in pairs(list) do count = count + 1 end

  --get the correct yStartingPoint for the staple
  startingPoint_y = yStartingPointStaple(startingPoint_y,count,height,space,maxEntriesPerColumn)

  --CREATE THE STAPLE
  counter = 0
  for i, names in ipairs(list) do
    if counter >= maxEntriesPerColumn then
      startingPoint_x = startingPoint_x + width + space
      counter = counter - maxEntriesPerColumn
    end
    table.insert(globalWiboxContainer,createOneMenuEntry(names, startingPoint_x, startingPoint_y + counter*(height + space), width, height, staple_level + 1))
    counter = counter + 1
  end

end

closeCsabiMenu = function()
  for i,value in ipairs(globalWiboxContainer) do
    value.content.screen = nil
    value.content.visible = false
    value.content.ontop = false
    value.content = nil
    globalWiboxContainer[i] = nil --THIS IS VERY IMPORTANT !! THE PREVIOUS COMMANDS DON'T DELETE THE ENTRY
  end
  keygrabber.stop()
end




abrakadabra = {}
for i = 1,10000 do
  abrakadabra[i] = wibox({})

  abrakadabra[i].visible = false
  abrakadabra[i] = nil
end
abrakadabra = nil


dofile(widgetPath .. "finishing.lua")
