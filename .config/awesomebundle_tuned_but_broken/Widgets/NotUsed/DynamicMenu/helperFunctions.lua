function getDirectoryEntries(path) -- returs table of absolute paths
  -- path must be path to a directory and ending with a slash!!
  local res = {}

  local str = awful.util.pread("ls -1 --file-type '" .. path .. "'")

  for line in string.gmatch(str, "[^\n]+") do
    table.insert(res, path .. line)
  end

  return res
end

function isDir(name)
  return string.sub(name,string.len(name),string.len(name)) == "/"
end

function makeNiceName(name)
  local local_name = ""

  if isDir(name) then
    local_name = string.match(name, "/[^/]*/[^/]*$") --/the/whole/name/ --> /name/
    local_name = string.sub(local_name, 2, string.len(local_name)-1) -- /name/ --> name
  else
    local_name = string.match(name, "/[^/]*$") --/the/whole/name --> /name
    local_name = string.sub(local_name, 2, string.len(local_name)) -- /name --> name
  end
  return "  " .. local_name
end

function yStartingPointStaple(startingPoint_y,count,height,space,maxEntriesPerColumn)
  oldStartingPoint_y = startingPoint_y
  if count > maxEntriesPerColumn then count = maxEntriesPerColumn end
  startingPoint_y = startingPoint_y - (count * (height+space))
  if startingPoint_y < 0 then startingPoint_y = entryHeight end
  return startingPoint_y + height
end

function shouldWeContinue(mod,key,event) --for Keygrabber
  if event ~= "press" then return true end
  if key=="Escape" then
    closeCsabiMenu()
    return false
  end
  return true
end

function size(list)
  --GET SIZE OF LIST
  count = 0
  for _ in pairs(list) do count = count + 1 end
  return count
end



function arangeEntries(entryList)
end


trashWiboxContainer = {}
