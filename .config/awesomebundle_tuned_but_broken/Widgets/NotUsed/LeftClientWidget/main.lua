leftClientsWibox = {}
rightClientsWibox = {}
leftHiddenClientWibox = {}
rightHiddenClientWibox = {}

for s=1,screen.count() do
  closeButton = wibox.widget.textbox()
  ontopButton = wibox.widget.textbox()
  stickyButton = wibox.widget.textbox()
  maximizeButton = wibox.widget.textbox()
  floatingButton = wibox.widget.textbox()

  closeButton:set_text(" X ")
  ontopButton:set_text(" O ")
  stickyButton:set_text(" S ")
  maximizeButton:set_text(" M ")
  floatingButton:set_text(" F ")

  closeButton:buttons(awful.util.table.join( awful.button( {}, 1, function() client.focus:kill() end )))
  ontopButton:buttons(awful.util.table.join( awful.button( {}, 1, function() client.focus.ontop = not client.focus.ontop end )))
  stickyButton:buttons(awful.util.table.join( awful.button( {}, 1, function() client.focus.sticky = not client.focus.sticky end )))
  maximizeButton:buttons(awful.util.table.join( awful.button( {}, 1, function() 
               if client.focus.maximized_horizontal ~= client.focus.maximized_vertical then
                 client.focus.maximized_vertical = client.focus.maximized_horizontal
               else
                 client.focus.maximized_vertical = not client.focus.maximized_vertical
                 client.focus.maximized_horizontal = not client.focus.maximized_horizontal
               end
             end )))
  floatingButton:buttons(awful.util.table.join( awful.button( {}, 1, function() awful.client.floating.toggle(client.focus) end )))

    rightClientsWibox[s]         = wibox({})
    rightClientsWibox[s].width   = 125
    rightClientsWibox[s].height  = 30
    rightClientsWibox[s].visible = true
    rightClientsWibox[s]:set_bg("#333333")
    rightClientsWibox[s].ontop = false

    rightClientsWibox[s].x       = screen[s].geometry.x + screen[s].geometry.width - rightClientsWibox[s].width
    rightClientsWibox[s].y       = screen[s].geometry.y



    local layout = wibox.layout.fixed.horizontal()
    layout:add(floatingButton)
    layout:add(maximizeButton)
    layout:add(stickyButton)
    layout:add(ontopButton)
    layout:add(closeButton)

    local layout2 = wibox.layout.align.horizontal()
    layout2:set_third(layout)

    rightClientsWibox[s]:set_widget(layout2)

end
