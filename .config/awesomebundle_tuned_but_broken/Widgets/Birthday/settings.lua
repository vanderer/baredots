birthdayPath = widgetPath .. "birthdays.csv"
birthdayIconText = ""
displayBirthdayIcons = true
birthdayIconSize = 70

birthdayIcons                 = {}
birthdayIcons.today           = widgetPath .. "cakeSmall.png"
birthdayIcons.notToday        = widgetPath .. "nocakeDarkSmall.png"
birthdayIcons.tooltipToday    = widgetPath .. "cake.png"
birthdayIcons.tooltipNotToday = widgetPath .. "nocakeDark.png"
