wlanColor                 = {}
wlanColor.fg              = easyTheme.lightColor
wlanColor.fg_urgent       = easyTheme.urgentColor

wlanColor.fg_title        = easyTheme.lightColor
wlanColor.fg_title_urgent = "#ff0000"
