wlanWidget = wibox.widget.background()

getWirelessInterface = function()
  local wirelessInterfaceText = awful.util.pread("iwconfig")
  local wirelessInterface = nil

  for line in string.gmatch(wirelessInterfaceText, "[^\n]+") do
    if string.find(line, "^%w") then
      if not string.find(line, "no wireless extensions") then
        wirelessInterface = string.match(line, "^%w+") 
      end
    end
  end

  return wirelessInterface
end

wlanFunction = function(widget, args)
  ssid = args["{ssid}"]
  link = args["{link}"]
  linp = args["{linp}"]
  if ssid == "N/A" then
    wlanWidget:set_fg(wlanColor.fg_urgent)
    wlanText      = "<span weight='bold' font='12' color='" .. wlanColor.fg_title_urgent .. "'>WLAN</span>\n\nNo WLAN Connection"
    if hadConnection then
      for s=1, screen.count() do
        naughty.notify({
                         screen    = s,
                         title     = "Wireless LAN",
                         text      = "No Connection",
                         icon      = widgetPath .. "wlan0.png",
                         icon_size = 30,
                         preset    = naughty.config.presets.critical,
                      })
      end
      hadConnection = false
    end
  else
    wlanWidget:set_fg(wlanColor.fg)
    wlanText      = "<span weight='bold' font='12' color='" .. wlanColor.fg_title .. "'>WLAN</span>\n\n" ..
                    "<b>SSID</b>\t<span color='" .. wlanColor.fg_urgent .."'>" .. ssid .. "</span>\n" .. 
                    "<b>Card</b>\t" .. wirelessInterface .. "\n" ..
                    "<b>Network</b>\t" .. ssid .. "\n" .. 
                    "<b>Strength</b>\t" .. link .. "/70 or " .. linp .. "%"
    if not hadConnection then
      for s=1, screen.count() do
        naughty.notify({
                         screen    = s,
                         title     = "Wireless LAN",
                         text      = "Connected to " .. ssid,
                         icon      = widgetPath .. "wlan5.png",
                         icon_size = 30,
                         preset    = naughty.config.presets.normal,
                      })
      end
      hadConnection = true
    end
  end
  return wlanText
end
