dofile(widgetPath .. "settings.lua")
dofile(widgetPath .. "functions.lua")

wirelessInterface = getWirelessInterface()

if wirelessInterface then
  dofile(widgetPath .. "textbox.lua")
  dofile(widgetPath .. "wibox.lua")

  hadConnection = false
  vicious.force({wlanTextbox})
end
