batFunction = function(widget, args)
  state  = args[1]
  amount = args[2]
  time   = args[3]

  if state == "+" or state == "↯" then
    state = "Charging"
    color = batColor.charging
    if batRemindTimer.started then
      batRemindTimer:stop()
    end
  elseif state == "⌁" then
    state = "Unknown"
    color = batColor.unknown
    if batRemindTimer.started then
      batRemindTimer:stop()
    end
  elseif tonumber(amount) > 20 then
    state = "Discharging"
    color = batColor.discharging
    if batRemindTimer.started then
      batRemindTimer:stop()
    end
  else
    state = "Urgent"
    color = batColor.urgent
    if not batRemindTimer.started then
      batRemindTimer:start()
    end
  end

  widget:set_color(color)

  local batText = "<span weight='bold' font='12' color='" .. color .. "'>Battery</span>\n\n" ..
                  "<b>State</b>\n" .. state .. "\n" ..
                  "<b>Amount</b>\n" .. amount .. "%\n" .. 
                  "<b>Time</b>\n" .. time .. "\n"
  batTextbox:set_markup(batText)

  batRemindTimerText = amount .. "% (" .. time .. ")"

  return amount
end
