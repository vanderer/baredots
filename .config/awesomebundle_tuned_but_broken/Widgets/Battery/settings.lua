batPBWidth = 5

batColor = {}
batColor.charging    = "#228b22"
batColor.unknown     = "#ffff00"
batColor.discharging = easyTheme.lightColor
batColor.urgent      = "#ff0000"

batIcon              = widgetPath .. "battery.png"
