awful.util.spawn("setxkbmap -option -layout " .. myKeyboardLayout .. " -variant " .. myKeyboardLayoutVariant .. " -option lv3:ralt_switch -option shift:both_capslock")
awful.util.spawn("feh --bg-scale " .. awesome_root .. "/Themes/" .. chosenTheme .. "/background.png")
awful.util.spawn("feh --bg-scale " .. awesome_root .. "/Themes/" .. chosenTheme .. "/background.jpg")
awful.util.spawn("xset -b")
