-- Initialize widget
homePartitionWidget = wibox.widget.textbox()
-- Register widget
vicious.register(homePartitionWidget, vicious.widgets.fs, "root: ${/home/ used_gb} / ${/home/ size_gb} G", 37)


homePartitionWidgetWibox = {}

for s = 1, screen.count() do
  homePartitionWidgetWibox[s] = wibox({})
  homePartitionWidgetWibox[s].height = 30
  homePartitionWidgetWibox[s].width = 250
  homePartitionWidgetWibox[s].visible = true

  wiboxPositioning(homePartitionWidgetWibox[s], s, viciousWiboxX, viciousWiboxY, "left", "top")

  local layout = wibox.layout.margin()
  layout:set_margins(8)
  layout:set_widget(homePartitionWidget)

  homePartitionWidgetWibox[s]:set_widget(layout)
end

