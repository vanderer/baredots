-- Initialize widget
memWidget = wibox.widget.textbox()
-- Register widget
vicious.register(memWidget, vicious.widgets.mem, "$1% ($2MB/$3MB)", 13)


memWidgetWibox = {}

for s = 1, screen.count() do
  memWidgetWibox[s] = wibox({})
  memWidgetWibox[s].height = 30
  memWidgetWibox[s].width = 250
  memWidgetWibox[s].visible = true

  wiboxPositioning(memWidgetWibox[s], s, viciousWiboxX, viciousWiboxY, "left", "top")

  local layout = wibox.layout.margin()
  layout:set_margins(8)
  layout:set_widget(memWidget)

  memWidgetWibox[s]:set_widget(layout)
end

