-- Initialize widget
fsWidget = wibox.widget.textbox()
-- Register widget
vicious.register(fsWidget, vicious.widgets.fs, "root: ${/ used_gb} / ${/ size_gb} G", 37)


fsWidgetWibox = {}

for s = 1, screen.count() do
  fsWidgetWibox[s] = wibox({})
  fsWidgetWibox[s].height = 30
  fsWidgetWibox[s].width = 250
  fsWidgetWibox[s].visible = true

  wiboxPositioning(fsWidgetWibox[s], s, viciousWiboxX, viciousWiboxY, "left", "top")

  local layout = wibox.layout.margin()
  layout:set_margins(8)
  layout:set_widget(fsWidget)

  fsWidgetWibox[s]:set_widget(layout)
end

