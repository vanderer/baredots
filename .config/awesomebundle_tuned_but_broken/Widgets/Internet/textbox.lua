internetTextbox = wibox.widget.textbox()
internetTextbox:set_markup("Setting up ...")
internetTextbox:set_font("Bitstream Vera Sans Mono, 8")
internetTextbox:set_valign("bottom")

internetWidget = wibox.widget.background()
internetWidget:set_widget(internetTextbox)
