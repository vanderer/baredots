internetWibox = {}

for s = 1, screen.count() do
  internetWibox[s] = wibox({})
  internetWibox[s].height = 150
  internetWibox[s].width = 250
  internetWibox[s].visible = true

  wiboxPositioning(internetWibox[s], s, 5, 0, "left", "top")

  local layout = wibox.layout.margin()
  layout:set_margins(8)
  layout:set_widget(internetWidget)

  internetWibox[s]:set_widget(layout)
end

