getInterfaces = function()
  local interfaces = {}

  interfacesText = awful.util.pread("ifconfig -a -s | awk '{print $1}' | tail -n +2")
  for line in string.gmatch(interfacesText, "[^\n]+") do
    if line ~= "lo" then
      table.insert(interfaces, line)
    end
  end
  return interfaces
end

-- interface: string containing interface
getInternalIP = function(interface)
    local ip = awful.util.pread("ip -4 -o addr | grep " .. interface .. " | awk '{print $4}' | awk -F/ '{print $1}'")
    local ip = string.sub(ip,1,string.len(ip)-1)
    return ip
end

-- interfaces: table of strings containing interfaces
getInternalIPs = function(interfaces)
  local ips = {}

  for _, interface in ipairs(interfaces) do
    local ip = getInternalIP(interface) 
    if ip ~= "" then
      ips[interface] = ip
    end
  end

  return ips
end

getExternalIP = function()
  local res = awful.util.pread("dig +short myip.opendns.com @resolver1.opendns.com")
  res = string.gsub(res,"%s","")
  bashCmdUpdateIPFile = "echo " .. res .. " >> " .. awesome_root .. "IPADDRESS"
  awful.util.spawn_with_shell(bashCmdUpdateIPFile)
  return res
end

internetFunction = function()
  haveInternet = false

  local ips = getInternalIPs(interfaces)

  if countAll(ips) > 0 then
    haveInternet = true
  end
  
  if haveInternet then 
    internetWidget:set_fg(internetColor.fg)
    local text = "<span weight='bold' font='12' color='" .. internetColor.fg_title .. "'>Internet</span>\n\nInternet Connection established\n\n<b>Internal IPs</b>"
    for _, interface in ipairs(interfaces) do
      if ips[interface] then
        text = text .. "\n" .. tostring(ips[interface]) .. " (" .. interface .. ")" 
      end
    end

    if not hadInternet then
      extIP = getExternalIP()
      for s=1, screen.count() do
      naughty.notify({
                       screen    = s,
                       title     = "Internet",
                       text      = "Internet Connection established",
                       icon      = widgetPath .. "internet.png",
                       icon_size = 30,
                       preset    = naughty.config.presets.normal,
                    })
      end
      hadInternet = true
      -- startProg("dropbox","start")
      startProg("megasync","")
    end
    if extIP then
      if string.find(extIP, "%d+%.%d+%.%d+%.%d+") then
        text = text .. "\n\n<b>External IP</b>\n" .. extIP
      else
        text = text .. "\n\n<b>External IP</b>\nProblem getting external IP"
      end
    end
    internetTextbox:set_markup(text)
  else
    internetWidget:set_fg(internetColor.fg_urgent)
    internetTextbox:set_markup("<span weight='bold' font='12' color='" .. internetColor.fg_title_urgent .. "'>Internet</span>\n\nNo Internet Connection")
    if hadInternet then
      extIP = nil
      for s=1, screen.count() do
        if s == mouse.screen then
          naughty.notify({
                           screen    = s,
                           title     = "Internet",
                           text      = "No Internet Connection",
                           timeout   = 0,
                           icon      = widgetPath .. "internetGray.png",
                           icon_size = 30,
                           preset    = naughty.config.presets.critical,
                        })
        else
          naughty.notify({
                           screen    = s,
                           title     = "Internet",
                           text      = "No Internet Connection",
                           icon      = widgetPath .. "internetGray.png",
                           icon_size = 30,
                           preset    = naughty.config.presets.critical,
                        })
        end
      end
      hadInternet = false
      -- stopProg("dropbox")
      stopProg("megasync")
    end
  end
end


