internetColor                 = {}
internetColor.fg              = easyTheme.lightColor
internetColor.fg_urgent       = easyTheme.urgentColor

internetColor.fg_title        = easyTheme.lightColor
internetColor.fg_title_urgent = easyTheme.urgentColor
