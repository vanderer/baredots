easyTheme = {}

easyTheme.lightColor    = "#00ffff"
easyTheme.darkColor     = "#000000aa"
easyTheme.neutralColor  = "#888888"
easyTheme.urgentColor  = "#ffff00"

easyTheme.borderColor   = "#000024"
easyTheme.fgColor   = "#ffffff"
--easyTheme.bgColor   = "#99efff"
easyTheme.bgColor   = "#40b6ff"

easyTheme.barWidth  = 60
