--Gather already opened windows to avoid rules being applied on them (they have already been positioned previously)
hasNeverAnyWindowBeenOpened = true

dofile("/home/csabi/.config/awesome/" .. "basicrules.lua")

basicrules.basicrules = {
  ------------------------------------------------------------
  { rule       = { },
    properties = { border_width     = beautiful.border_width,
                   --floating         = true,
                   border_color     = beautiful.border_normal,
                   size_hints_honor = false,
                   focus            = true,
                   keys             = clientkeys,
                   buttons          = clientbuttons },
                 
    callback   = function(c)      
                   local titlebars_enabled = true
                   if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
                       local space = wibox.widget.textbox(" ")
                       -- Widgets that are aligned to the left
                       local left_layout = wibox.layout.fixed.horizontal()
                       left_layout:add(awful.titlebar.widget.iconwidget(c))
                       left_layout:add(space)

                       -- Widgets that are aligned to the right
                       local right_layout = wibox.layout.fixed.horizontal()
                       right_layout:add(awful.titlebar.widget.floatingbutton(c))
                       right_layout:add(awful.titlebar.widget.maximizedbutton(c))
                       right_layout:add(awful.titlebar.widget.stickybutton(c))
                       right_layout:add(awful.titlebar.widget.ontopbutton(c))
                       right_layout:add(awful.titlebar.widget.closebutton(c))

                       -- The title goes in the middle
                       local title = awful.titlebar.widget.titlewidget(c)
                       title:buttons(awful.util.table.join(
                               awful.button({ }, 1, function()
                                   client.focus = c
                                   c:raise()
                                   awful.mouse.client.move(c)
                               end),
                               awful.button({ }, 3, function()
                                   client.focus = c
                                   c:raise()
                                   awful.mouse.client.resize(c)
                               end)
                               ))

                       -- Now bring it all together
                       local layout = wibox.layout.align.horizontal()
                       layout:set_left(left_layout)
                       layout:set_right(right_layout)
                       layout:set_middle(title)

                       awful.titlebar(c):set_widget(layout)
                   end
                 end
                 },
  ------------------------------------------------------------
}
client.connect_signal("manage", basicrules.apply)
client.disconnect_signal("manage", awful.rules.apply)

awful.rules.rules = {
  ------------------------------------------------------------
  { rule       = { },
    properties = { border_width     = beautiful.border_width,
                   border_color     = beautiful.border_normal,
                   size_hints_honor = false,
                   focus            = true,
                   keys             = clientkeys,
                   buttons          = clientbuttons },
                 
    --callback   = function(c)
                   --awful.titlebar.add(c, { modkey = modkey, bg_focus = beautiful.client_bg_focus })
                 --end
    callback   = function(c)      
                   local titlebars_enabled = true
                   if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
                       local space = wibox.widget.textbox(" ")
                       -- Widgets that are aligned to the left
                       local left_layout = wibox.layout.fixed.horizontal()
                       left_layout:add(awful.titlebar.widget.iconwidget(c))
                       left_layout:add(space)

                       -- Widgets that are aligned to the right
                       local right_layout = wibox.layout.fixed.horizontal()
                       right_layout:add(awful.titlebar.widget.floatingbutton(c))
                       right_layout:add(awful.titlebar.widget.maximizedbutton(c))
                       right_layout:add(awful.titlebar.widget.stickybutton(c))
                       right_layout:add(awful.titlebar.widget.ontopbutton(c))
                       right_layout:add(awful.titlebar.widget.closebutton(c))

                       -- The title goes in the middle
                       local title = awful.titlebar.widget.titlewidget(c)
                       --title.text:set_text("Hallo")
                       title:buttons(awful.util.table.join(
                               awful.button({ }, 1, function()
                                   client.focus = c
                                   c:raise()
                                   awful.mouse.client.move(c)
                               end),
                               awful.button({ }, 3, function()
                                   client.focus = c
                                   c:raise()
                                   awful.mouse.client.resize(c)
                               end)
                               ))

                       -- Now bring it all together
                       local layout = wibox.layout.align.horizontal()
                       layout:set_left(left_layout)
                       layout:set_right(right_layout)
                       layout:set_middle(title)

                       awful.titlebar(c):set_widget(layout)
                   end
                 end
                 },
  ------------------------------------------------------------
  { rule_any   = { class = { "Plank" } },
    properties = { floating = true }, 
    callback   = function(c)
                   --awful.placement.centered(c)
                   naughty.notify({text="Plank has been started"})
                   c:struts( { bottom = 100 } )
                 end },
  ------------------------------------------------------------
  { rule_any   = { class = { "Gxmessage", "Xmessage" } },
    properties = { floating = true }, 
    callback   = function(c)
                   awful.placement.centered(c)
                 end },
  ------------------------------------------------------------
  { rule       = { class = "URxvt" },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][3]}) 
                   awful.tag.viewonly(tags[mouse.screen][3])
                 end },
  ------------------------------------------------------------
  --{ rule       = { string.find(name, "INPUT") },
    --properties = { floating = true, fullscreen=false, maximized_vertical=false maximized_horizontal=false}, 
    --callback   = function(c) 
                   --c.maximized_vertical = false
                   --c.maximized_horizontal = false
                   --awful.client.floating.set(c, false)
                 --end },

  { rule       = { class = { "Gvim" } },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][4]}) 
                   awful.tag.viewonly(tags[mouse.screen][4])
                 end },

  ------------------------------------------------------------
  { rule_any   = { class = { "ROX-Filer" } },
    properties = { floating = true,
                 },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][7]}) 
                   c:set_height(300)
                   c:set_width(300)
                   awful.tag.viewonly(tags[mouse.screen][7])
                 end },
  ------------------------------------------------------------
  { rule_any   = { class = { "Chromium" } },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][7]}) 
                   awful.tag.viewonly(tags[mouse.screen][7])
                 end },
  ------------------------------------------------------------
  { rule_any   = { class = { "Dwb", "Firefox" } },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][2]}) 
                   awful.tag.viewonly(tags[mouse.screen][2])
                 end },
  ------------------------------------------------------------
  { rule_any   = { class = { "Evince",
                             "Zathura" } },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][5]}) 
                 end },
  ------------------------------------------------------------   
  { rule       = { class = "Marlin" },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][6]}) 
                   awful.tag.viewonly(tags[mouse.screen][6])
                 end },
  ------------------------------------------------------------   
  { rule_any   = { class = { "libreoffice-startcenter", 
                             "libreoffice-writer", 
                             "libreoffice-calc", 
                             "libreoffice-impress" } },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][1]}) 
                 end },
  ------------------------------------------------------------   
  { rule       = { class = "Kmymoney" },
    callback   = function(c) 
                   c:tags({tags[mouse.screen][1]}) 
                 end },
  { rule       = { class = "Kmymoney", name = "Schedules" },
    properties = { floating = true}, 
    callback   = function(c) 
                   c:tags({tags[mouse.screen][1]}) 
                 end },
  ------------------------------------------------------------   
  { rule       = { class = "Gimp" },
    properties = { floating = true}, 
    callback   = function(c) 
                   --c:tags({tags[mouse.screen][9]}) 
                 end },
  ------------------------------------------------------------   
}
