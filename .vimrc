""""""""""""""""""""""
" begin basic settings
""""""""""""""""""""""
	set nocompatible " be iMproved, required
	let mapleader=',' "mapleader settings
	filetype on " required
	autocmd BufNewFile,BufRead *.tex set filetype=tex "recognize tex documents when creating them
	autocmd Filetype tex setl updatetime=1 "recognize tex documents when creating them
	autocmd BufNewFile,BufRead *.ms set filetype=groff "recognize groff documents when creating them
	set autoindent smartindent "keep indent level but decrease it, when syntax requires it.
	set ts=2 "tabstop width
	set sw=2 "shift width
	set path+=** "search for files in all subdirectories
	set wildmenu
	set visualbell
	set linebreak "break long lines
	set smartcase "search only case sensitive if user uses capitalization
	set scrolloff=5
	set nu "show numbers at the side of the file
	set foldmethod=indent "close folds based on deeper indentation
	set foldenable "enable folding...
	set foldlevelstart=99 " ... but don't fold by default
	set guioptions="" "hide all gui elements, show just the editor
	set guifont=Fira\ Code\ 12
""""""""""""""""""""
" end basic settings
""""""""""""""""""""

"""""""""""""""""""""""
" begin  basic mappings
"""""""""""""""""""""""
  " initialize a basic school document
	map <F5> i\include{/home/csabi/.csonfig/Resources/Latex/schule.tex}\topicol{0061a9}% M1:cd0248% M2:0061a9% M3:008800% D1:44ff88% D2:aa9900\begin{document}\end{document}kki
	map <F6> i\include{/home/csabi/.csonfig/Resources/Latex/beamer.tex}\begin{document}\end{document}kki

	" gui font size mappings:
	map <F9> set guifont=Fira\ Code\ 12
	map <F10> set guifont=Fira\ Code\ 14
	map <F11> set guifont=Fira\ Code\ 16
	map <F12> set guifont=Fira\ Code\ 18
""""""""""""""""""""
" end basic mappings
""""""""""""""""""""

"""""""""""""""""
call plug#begin()
"""""""""""""""""
	Plug 'felipec/notmuch-vim' "mutt support
	Plug 'itchyny/lightline.vim' "powerline
	Plug 'dharanasoft/rtf-highlight' "highlight rich text format
	Plug 'dpelle/vim-LanguageTool' "correct spelling and grammar
	Plug 'vim-pandoc/vim-pandoc' "integrate markdown
	Plug 'vim-pandoc/vim-pandoc-syntax' "syntax highlight markdown
	Plug 'MarcWeber/vim-addon-mw-utils' "dependency of vim-snipmate
	Plug 'garbas/vim-snipmate' "ability to complete snippets
	Plug 'honza/vim-snippets' "library of all the snippets
	Plug 'tomtom/tlib_vim' "dependency of vim-snippets
	Plug 'mbbill/undotree' "if you want to redo an undo but you did already something else and redoing would mean doing something else instead of what you originally did.
	Plug 'scrooloose/nerdcommenter' "comment smartly
	"Plug 'preservim/nerdtree' "comment smartly
	Plug 'tomasr/molokai' "colorscheme
	Plug 'nanotech/jellybeans.vim' "colorscheme
	Plug 'balanceiskey/vim-framer-syntax' "colorscheme
	Plug 'rafi/awesome-vim-colorschemes' "colorscheme
	Plug 'vim-scripts/AutoComplPop' "get completion suggestions while typing
	Plug 'vimwiki/vimwiki' "create easily an entire wiki with vim
	Plug 'tpope/vim-repeat' "make the . command repeat entire plugin command combos
	Plug 'tpope/vim-surround' "make pretty boxes around things
	Plug 'tmhedberg/matchit' " make % jump not only between ( ... ) but also between <tag> ... </tag>
	Plug 'sickill/vim-pasta' "paste with correct indentation
	Plug 'scrooloose/syntastic' "complain about my code being not correct or not quite pretty enough
	Plug 'rstacruz/sparkup' "magically create documents of certain structures (useful for html and other markups)
"""""""""""""""
call plug#end()
"""""""""""""""

""""""""""""""""""""""""
" begin plugin variables
""""""""""""""""""""""""
	" languagetool
	let g:languagetool_jar='/home/csabi/.csonfig/Scripts/LanguageTool-4.8-stable/languagetool-commandline.jar'
	let g:languagetool_lang='de-CH'

	" vimwiki path
	let g:vimwiki_list = [{'path': '~/.csonfig/Resources/Reference/vimwiki/'}]

	" snipmate
	let g:snipMate = get(g:, 'snipMate', {}) " Allow for vimrc re-sourcing
	let g:snipMate = { 'snippet_version' :1 }
	colorscheme framer_syntax_dark

	" groff
	" inoremap  :s/^\.[^ ]*/\U&/eA " somehow this prevents breaking
	" lines in the middle

	" Exit Vim if NERDTree is the only window remaining in the only tab.
	autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
""""""""""""""""""""""
" end plugin variables
""""""""""""""""""""""
