"""""""""""""""""""'
" APPEARANCE
"""""""""""""""""""'
"colorscheme molokai
colorscheme hybrid_reverse
set guioptions=""
set guifont=Bitstream\ Vera\ Sans\ Mono\ 12
set nu
set relativenumber
autocmd InsertLeave * :set relativenumber
syntax on
set expandtab
set ts=2
set sw=2
set scrolloff=5


"""""""""""""""""""'
" EASE OF USE
"""""""""""""""""""'
set foldmethod=indent 
set hlsearch
let g:snipMate = get(g:, 'snipMate', {}) " Allow for vimrc re-sourcing
let g:livepreview_previewer = 'zathura'
let g:livepreview_engine = 'xelatex'
autocmd Filetype tex setl updatetime=1
set smartindent
set path+=**
set wildmenu
set vb
set formatoptions+=tcql
set lbr
set smartcase
nmap gw <c-w>w
nmap gW <c-w>W
