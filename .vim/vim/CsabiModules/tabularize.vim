imap <silent>,: <Esc>:Tabularize /:A
imap <silent>:: <Esc>:Tabularize /::A
imap <silent>,\| <Esc>:Tabularize /\|A
imap <silent>,= <Esc>:Tabularize /=A
imap <silent>,& <Esc>:Tabularize /&A

"imap <S-Enter> :Tabularize /\| :s/\v(\|([- ]*)\|)/\='+'.repeat('-',strlen(submatch(2))).'+'/g
