" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
"""""""""""""""""""'
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

"""""""""""""""""""'
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" plugin on GitHub repo

"Plugin 'dhruvasagar/vim-table-mode'
"Plugin 'plasticboy/vim-markdown'
"Plugin 'MarcWeber/vim-addon-mw-utils'
"Plugin 'hsitz/VimOrganizer'
"Plugin 'tomtom/tlib_vim'
"Plugin 'vim-pandoc/vim-pandoc'
"Plugin 'vim-pandoc/vim-pandoc-syntax'
"Plugin 'kristijanhusak/vim-hybrid-material'
"Plugin 'godlygeek/tabular'

Plugin 'bling/vim-airline'
Plugin 'dharanasoft/rtf-highlight'
Plugin 'easymotion/vim-easymotion'
Plugin 'ganwell/vim-hunspell-dicts'
Plugin 'garbas/vim-snipmate'
Plugin 'gosukiwi/vim-atom-dark'
Plugin 'honza/vim-snippets'
Plugin 'mbbill/undotree'
Plugin 'prettier/vim-prettier'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'tomasr/molokai'
Plugin 'vim-scripts/AutoComplPop'
Plugin 'vim-scripts/YankRing.vim'
Plugin 'vimwiki/vimwiki'
Plugin 'xuhdev/vim-latex-live-preview'

Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-vividchalk'

Plugin 'bronson/vim-trailing-whitespace'
Plugin 'tmhedberg/matchit'
Plugin 'sickill/vim-pasta'
Plugin 'scrooloose/syntastic'
Plugin 'ervandev/supertab'
Plugin 'vim-scripts/ZoomWin'
Plugin 'rstacruz/sparkup'

"""""""""""""""""""'
" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
"""""""""""""""""""'
call vundle#end()            " required
"""""""""""""""""""'
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
" 
"""""""""""""""""""'
"Activation of plugins
"""""""""""""""""""'
"and other stuff
"""""""""""""""""""'

