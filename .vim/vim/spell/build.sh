#!/bin/sh

set -e

build () {
    rm -rf $1
    mkdir $1
    vim -c "mkspell! $1/hun-de-CH $2/hun-de-CH.utf-8.spl | qa"
    vim -c "mkspell! $1/hun-de-DE $2/hun-de-DE.utf-8.spl | qa"
    vim -c "mkspell! $1/hun-de-AT $2/hun-de-AT.utf-8.spl | qa"
    vim -c "mkspell! $1/hun-be-BY $2/hun-be-BY.utf-8.spl | qa"
    vim -c "mkspell! $1/hun-de-CH-frami $2/hun-de-CH-frami.utf-8.spl | qa"
    vim -c "mkspell! $1/hun-de-DE-frami $2/hun-de-DE-frami.utf-8.spl | qa"
    vim -c "mkspell! $1/hun-de-AT-frami $2/hun-de-AT-frami.utf-8.spl | qa"
}

for lang in de_CH de_DE de_AT be_BY; do
    cat $lang-max3.aff | \
    sed 's/COMPOUNDRULE xy?z/COMPOUNDRULE xy*z/' | \
    sed 's/COMPOUNDWORDMAX 3//' > $lang-nomax.aff
done

build max3out max3
build nomaxout nomax
