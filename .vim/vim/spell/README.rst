hunspell dictionaries for Vim
=============================

I will accept requests for other languages, given the person requesting will
help testing.

German
------

Patched germen hunspell dictionaries for Vim. Vim is missing some of the
hunspell features, I tried to fix the aff file to use the most of Vims
spell-check-capabilities.

The most important improvement: German compound words are recognized. There are
two flavours of the dictionaries.

- The standard will only accept a max of 3 compound words
- The nomax will accept a word like: Donaudampfschifffahrtskapitänskajütentür

Belarusian
----------

Created with support of the original author:

Мікалай Удодаў <crom-a@tut.by>

and help and testing by:

Viktar Siarheichyk http://vics.eq.by

Know issues
...........

I couldn't translate the character-mappings (characters that are more or
less the same) 100% accurate.


Install
-------

- Download: http://1042.ch/spell/
- Save to: ~/.vim/spell/
- Load with: set spell spelllang=hun-de-CH-frami

Build
-----

./build.sh
